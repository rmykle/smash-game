import React, { Component } from "react";
import "./App.scss";
import UserNameSelect from "./components/UserNameSelect";
import LobbySelector from "./components/LobbySelector";
import Lobby from "./components/Lobby";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lobby: undefined,
      currentUser: "",
      gameMessage: "",
      error: "",
      lobbies: [],
      taskData: {}
    };
    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {
    //this.connection = new WebSocket("ws://158.39.77.136:5000");
    this.connection = new WebSocket("wss://www.ssr.myklevoll.com/server");
    this.connection.onmessage = message => {
      //console.log(message.data);

      const { type, data } = JSON.parse(message.data);
      //console.log(data);

      if (type === "clientInit") {
        this.setState({
          currentUser: data.validUserName,
          lobbies: data.lobbies
        });
      } else if (type === "lobby_data")
        this.setState({ lobby: data }, () => console.log("SATET", this.state));
      else if (type === "lobby_updated") this.setState({ lobbies: data });
      else if (type === "game_message") this.setState({ gameMessage: data });
      else if (type === "new_assignment") this.setState({ taskData: data });
    };

    this.connection.onclose = () => {
      this.setState({ error: "Lost connection to host" });
    };
  }
  render() {
    if (this.state.error) return <>{this.state.error}</>;
    else if (!this.state.currentUser)
      return <UserNameSelect sendMessage={this.sendMessage} />;
    else if (this.state.lobby === undefined)
      return (
        <LobbySelector
          lobbies={this.state.lobbies}
          sendMessage={this.sendMessage}
        />
      );

    return (
      <Lobby
        lobby={this.state.lobby}
        sendMessage={this.sendMessage}
        gameMessage={this.state.gameMessage}
        taskData={this.state.taskData}
      />
    );
  }

  sendMessage(message) {
    this.connection.send(message);
  }
}

export default App;
