import React, { Component } from "react";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      background: ""
    };
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);

    this.mouseDown = false;
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown);
    document.addEventListener("keyup", this.handleKeyUp);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
    document.removeEventListener("keyup", this.handleKeyUp);
  }

  handleKeyDown(event) {
    if (!this.mouseDown) {
      this.mouseDown = true;
      if (event.key === "ArrowUp") this.submit(0);
      else if (event.key === "ArrowRight") this.submit(1);
      else if (event.key === "ArrowDown") this.submit(2);
      else if (event.key === "ArrowLeft") this.submit(3);
    }
  }
  handleKeyUp(event) {
    this.mouseDown = false;
  }

  render() {
    //console.log(this.props.taskData);

    return (
      <div className={`game ${this.state.background}`}>
        <h5>Points: {this.props.taskData.roundPoints}</h5>
        <div className="pad">
          <div>
            <i className="material-icons" onClick={() => this.submit(0)}>
              arrow_drop_up
            </i>
          </div>
          <div>
            <i className="material-icons" onClick={() => this.submit(3)}>
              arrow_left
            </i>

            <i className="material-icons guessIcon">
              {`arrow_${this.convertIcon(this.props.taskData.currentTask)}`}
            </i>

            <i className="material-icons" onClick={() => this.submit(1)}>
              arrow_right
            </i>
          </div>
          <div>
            <i className="material-icons" onClick={() => this.submit(2)}>
              arrow_drop_down
            </i>
          </div>
        </div>
      </div>
    );
  }

  submit(num) {
    const correct = this.props.taskData.currentTask === num;
    const bg = correct ? "correct" : "wrong";
    this.props.sendMessage(
      JSON.stringify({ type: "submit_answer", data: num })
    );

    this.setState({ background: bg }, () => {
      setTimeout(() => {
        this.setState({ background: "" });
      }, 100);
    });
  }

  convertIcon(num) {
    switch (num) {
      case 0:
        return "drop_up";
      case 1:
        return "right";
      case 2:
        return "drop_down";
      case 3:
        return "left";
      default:
        break;
    }
  }
}
