import React, { Component } from "react";
import LobbyTable from "./LobbyTable";
import Game from "./Game";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = { inRound: false };
  }
  render() {
    const { gameMessage } = this.props;
    const {
      initiated,
      isAdmin,
      currentRound,
      totalRounds,
      players,
      roundRunning,
      gameComplete
    } = this.props.lobby;
    return (
      <div className="ingame">
        <h1>Smash</h1>
        {initiated ? (
          <h3>
            Round {currentRound} / {totalRounds}
          </h3>
        ) : null}
        <h4>{gameMessage}</h4>
        {!roundRunning ? (
          <LobbyTable
            players={players}
            initiated={initiated}
            gameComplete={gameComplete}
          />
        ) : (
          <Game
            taskData={this.props.taskData}
            sendMessage={this.props.sendMessage}
          />
        )}
        <div>
          {isAdmin && !initiated ? (
            <button
              onClick={() =>
                this.props.sendMessage(JSON.stringify({ type: "start_game" }))
              }
            >
              Start game
            </button>
          ) : null}
          <button
            onClick={() =>
              this.props.sendMessage(JSON.stringify({ type: "leave_lobby" }))
            }
          >
            Leave lobby
          </button>
        </div>
      </div>
    );
  }
}
