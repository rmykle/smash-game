import React, { Component } from "react";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = { newRoundNumber: 3, newRoundNumberOptions: [3, 5, 10] };
  }

  render() {
    const { lobbies, sendMessage } = this.props;
    return (
      <div className="lobbySelector">
        <h1>Lobbies</h1>
        {lobbies.length ? (
          lobbies.map(lobby => (
            <div className="lobby" key={lobby.lobbyID}>
              <h5>
                Host: {lobby.hostname} - Players: {lobby.players}
              </h5>
              <button
                onClick={() =>
                  sendMessage(
                    JSON.stringify({ type: "join_lobby", data: lobby.lobbyID })
                  )
                }
              >
                Join
              </button>
            </div>
          ))
        ) : (
          <h5>No lobbies yet :( </h5>
        )}
        <div className="newLobby">
          <h3>New lobby</h3>
          <div>
            {this.state.newRoundNumberOptions.map(option => (
              <label key={option}>
                <input
                  onChange={() => this.setState({ newRoundNumber: option })}
                  id={option}
                  type="radio"
                  checked={option === this.state.newRoundNumber}
                />
                {option} rounds
              </label>
            ))}
          </div>
          <input
            type="submit"
            value="Create lobby"
            onClick={() =>
              sendMessage(
                JSON.stringify({
                  type: "create_lobby",
                  data: this.state.newRoundNumber
                })
              )
            }
          />
        </div>
      </div>
    );
  }
}
