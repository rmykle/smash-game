import React from "react";

export default ({ players, initiated, gameComplete }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          {initiated || gameComplete ? (
            <>
              <th>Round</th>
              <th>Total</th>
            </>
          ) : null}
        </tr>
      </thead>
      <tbody>
        {players.sort((a, b) => b.points - a.points).map((player, i) => (
          <tr key={i}>
            <td>{player.name}</td>
            {initiated || gameComplete ? (
              <>
                <td>{player.roundPoints}</td>
                <td>{player.totalPoints}</td>
              </>
            ) : null}
          </tr>
        ))}
      </tbody>
    </table>
  );
};
