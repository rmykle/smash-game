import React, { Component } from "react";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = { nameInput: "", error: "" };
  }

  render() {
    return (
      <div className="selectName">
        <h1>Nickname:</h1>

        <form onSubmit={e => this.join(e)}>
          <input
            type="text"
            value={this.state.nameInput}
            onChange={e => this.setState({ nameInput: e.target.value })}
          />
          <input className="submitName" type="submit" value="Join" />
        </form>

        {this.state.error ? <h5>{this.state.error}</h5> : null}
      </div>
    );
  }
  join(e) {
    e.preventDefault();
    const { nameInput } = this.state;
    if (nameInput.length < 2)
      this.setState({ error: "Too short.. Minimum 2 characters" });
    else
      this.props.sendMessage(
        JSON.stringify({ type: "name_selected", data: nameInput })
      );
  }
}
