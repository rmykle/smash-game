let connection = undefined;
if (!connection) connection = new WebSocket("ws://localhost:5000");

let appStateProxy = undefined;
let lobbyStateProxy = undefined;

connection.onmessage = message => {
  console.log(message.data);

  const { type, data } = JSON.parse(message.data);
  console.log(data);

  if (type === "clientInit") {
    this.setState({ currentUser: data.validUserName, lobbies: data.lobbies });
  } else if (type === "joined_lobby")
    this.setState({ lobbyID: data }, () => console.log("SATET", this.state));
  else if (type === "lobby_updated") this.setState({ lobbies: data });
};

this.connection.onclose = () => {
  if (this.connection.lobbyID) {
  }
  this.setState({ error: "Lost connection to host" });
};
