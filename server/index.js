const WebSocketServer = require("websocket").server;
const http = require("http");

let lobbyID = 0;

class Lobby {
  constructor(host, totalRounds) {
    this.host = host;
    this.players = [];
    this.lobbyID = lobbyID++;
    this.totalRounds = totalRounds;
    this.currentRound = 0;
    this.roundTime = 15;
    this.initiated = false;
    this.gameComplete = false;
    this.roundRunning = false;
  }

  addPlayer(connection) {
    this.players.push(connection);
    connection.gameData = { points: 0, lobby: this };
    this.players.forEach(player => player.sendUTF(this.toJson(player)));
  }

  removePlayer(connection) {
    this.players = this.players.filter(player => player != connection);
    connection.gameData = undefined;
    connection.sendUTF(JSON.stringify({ type: "lobby_data", data: undefined }));
    this.broadCastGameData();
    broadCastLobbies();
  }

  startGame() {
    this.initiated = true;
    this.players.forEach(player => (player.gameData.totalPoints = 0));
    this.broadCastGameData();
    const messageQueue = ["Game is booting up!", "Ready", "Set", "Go!"];
    let i = 0;

    const game = this;

    const messageInterval = setInterval(function() {
      game.players.forEach(player =>
        player.sendUTF(
          JSON.stringify({ type: "game_message", data: messageQueue[i] })
        )
      );
      i++;
      if (i === messageQueue.length) {
        clearInterval(messageInterval);
        game.initGameRounds();
      }
    }, 1000);
  }

  initGameRounds() {
    this.gameComplete = false;
    this.currentRound = 0;
    this.runRound();
  }
  runRound() {
    const gameArray = new Array(150)
      .fill(0)
      .map(() => Math.round(Math.random() * 3));
    this.players.forEach(player => {
      player.gameData.gameArray = gameArray.slice();
      player.gameData.currentIndex = 0;
      player.gameData.roundPoints = 0;
      sendNextTask(player);
    });

    let seconds = 0;
    this.currentRound++;
    this.roundRunning = true;
    this.broadCastGameData();
    const game = this;

    const round = setInterval(function() {
      // Init round interval
      seconds++;
      game.sendGameMessage("Time left: " + (game.roundTime - seconds));

      if (seconds === game.roundTime) {
        // Round ended
        clearInterval(round);
        game.roundRunning = false;
        game.players.forEach(
          player => (player.gameData.totalPoints += player.gameData.roundPoints)
        );
        game.broadCastGameData();
        if (game.currentRound < game.totalRounds) {
          // Check if last round
          let breakTime = 10;
          const breakTimer = setInterval(function() {
            game.sendGameMessage("New round in: " + breakTime);
            breakTime--;
            if (breakTime < 0) {
              // Breaks done
              clearInterval(breakTimer);
              game.runRound();
            }
          }, 1000);
        } else {
          game.initiated = false;
          game.gameComplete = true;
          game.sendGameMessage("Game over!");
          game.broadCastGameData();
        }
      }
    }, 1000);
  }

  sendGameMessage(message) {
    this.players.forEach(player =>
      player.sendUTF(
        JSON.stringify({
          type: "game_message",
          data: message
        })
      )
    );
  }

  broadCastGameData() {
    this.players.forEach(player => player.sendUTF(this.toJson(player)));
  }

  toJson(player) {
    return JSON.stringify({
      type: "lobby_data",
      data: {
        lobbyID: this.lobbyID,
        currentRound: this.currentRound,
        totalRounds: this.totalRounds,
        initiated: this.initiated,
        gameComplete: this.gameComplete,
        roundRunning: this.roundRunning,
        isAdmin: player === this.host,
        players: this.players.map(player => {
          return {
            name: player.userName,
            roundPoints: player.gameData.roundPoints,
            totalPoints: player.gameData.totalPoints
          };
        })
      }
    });
  }
}

let lobbies = [];

const server = http.createServer({
  function(request, response) {
    //Empty for now
  }
});
server.listen(5000, function() {
  console.log("Server init at port 5000");
});

webSocketServer = new WebSocketServer({ httpServer: server });
webSocketServer.on("request", function(request) {
  const connection = request.accept(null, request.origin);
  connection.on("message", function(utfmessage) {
    const message = JSON.parse(utfmessage.utf8Data);

    if (message.type === "create_lobby") {
      createLobby(connection, message.data);
    } else if (message.type === "submit_answer") {
      sendNextTask(connection, message.data);
    } else if (message.type === "join_lobby") {
      const selectedLobby = lobbies.filter(
        lobby => lobby.lobbyID === message.data
      )[0];
      selectedLobby.addPlayer(connection);
      broadCastLobbies();
    } else if (message.type === "leave_lobby") {
      leaveLobby(connection);
    } else if (message.type === "start_game") {
      connection.gameData.lobby.startGame();
    } else if (message.type === "name_selected") {
      onJoinMain(connection, message.data);
    }
  });
  connection.on("close", function() {
    leaveLobby(connection);
  });
});

function createLobby(connection, totalRounds) {
  const lobby = new Lobby(connection, totalRounds);
  lobbies.push(lobby);
  lobby.addPlayer(connection);
  broadCastLobbies();
}

function leaveLobby(connection) {
  if (!connection.gameData) return;
  const currentLobby = connection.gameData.lobby;
  currentLobby.removePlayer(connection);
  if (currentLobby && currentLobby.players.length === 0) {
    lobbies = lobbies.filter(lobby => currentLobby !== lobby);
  }
  broadCastLobbies();
}

function broadCastLobbies() {
  webSocketServer.connections
    .filter(player => !player.gameData)
    .forEach(player =>
      player.sendUTF(
        JSON.stringify({ type: "lobby_updated", data: lobbiesFormatted() })
      )
    );
}

function lobbiesFormatted() {
  return lobbies.map(lobby => {
    return {
      lobbyID: lobby.lobbyID,
      hostname: lobby.host.userName,
      players: lobby.players.length
    };
  });
}

function sendNextTask(connection, previousAnswer) {
  const { currentIndex, gameArray } = connection.gameData;

  if (currentIndex > 0) {
    if (previousAnswer === gameArray[currentIndex - 1]) {
      connection.gameData.roundPoints += 1;
    } else {
      connection.gameData.roundPoints -= 1;
    }
  }

  connection.sendUTF(
    JSON.stringify({
      type: "new_assignment",
      data: {
        currentTask:
          connection.gameData.gameArray[connection.gameData.currentIndex],
        roundPoints: connection.gameData.roundPoints,
        points: connection.gameData.totalpoints
      }
    })
  );

  connection.gameData.currentIndex += 1;
}

function onJoinMain(connection, name) {
  connection.userName = name;
  connection.sendUTF(
    JSON.stringify({
      type: "clientInit",
      data: {
        validUserName: connection.userName,
        lobbies: lobbiesFormatted()
      }
    })
  );
}

let i = 0;
//setInterval(function() {webSocketServer.broadcast("HEI " + i++)}, 1000)
